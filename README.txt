DESCRIPTION
-----------

The ApacheSolr SEO blocks provides some extra blocks that improve the user
experience of filtering and the positioning of the ApacheSolr pages result of a
combination of multiple active filters in the facets. The blocks provided get
the values and types of the activated filters providing two useful blocks:

- Block with Active Filters: this block give a list with the names of the
  activated blocks so you can have together all the important keywords of the
page that a user navigate. Positioning this block in the header of a page make
it very semantic for search engines and users that easily can deactivate a
filter. 
- Block Descriptions of Active Filters: this block return a list of
  descriptions for every activated filter currently supporting taxonomy terms,
nodereferences and node types. This is very useful to add a header description
of related with all the results the user is browsing. For taxonomy terms and
node types filters we use the description fields for that objects and for
nodereferences the description field is configurable via block form
configuration. The combination of multiple filters display all the descriptions
in a list so with well writed descriptions the search engines have useful
keyword to relate for every different faceted page.

INSTALLATION
------------

- Install the module as usual.
- Activate the blocks seo_blocks_active_filters and seo_blocks_description in
  the results pages of ApacheSolr search. You can do it via default Drupal
blocks management, via Contexts or via Panels.
- For the seo_blocks_description if you have an active facet of type
  nodereference you will provide the name of CCK field from which the
description is retrieved. Got to the block configuration.
admin/build/block/configure/apachesolr_seo_blocks/seo_blocks_description
- Go to you ApacheSolr result pages and activate some filters. Look that the
  block seo_blocks_active_filters will display a list with the names of that
filters with the option of deactivate or clear all active filters. Also you
will have a block that display the descriptions for all the activated filters.
To see the blocks is needed to activate a filter.

CUSTOMIZATION
-------------

Customize the descriptions source

To provide your own descriptions for the Block Descriptions you can alter the
callbacks that are the responsible to retrieve and return the rendered
description for an active filter.

  function hook_apachesolr_seo_blocks_descriptions_info_alter($callbacks) {
    $callbacks = array(
      // callback for the content_taxonomy filters
      'content_taxonomy' => array(
        'description_callback' => 'apachesolr_seo_blocks_taxonomy_term_description',
        'weight' => 1,
      ),
      // callback for the node_type filters
      'node_type' => array(
        'description_callback' => 'apachesolr_seo_blocks_node_type_description',
        'weight' => -1,
      ),
      // callback for nodereference node_type filters
      'nodereference' => array(
        'description_callback' => 'apachesolr_seo_blocks_nodereference_description',
        'weight' => 0,
      ),
    );
  }

Defining your custom callback can override the logic from which the description
is get. An example of the implementation of a callback function:

  function apachesolr_seo_blocks_taxonomy_term_description($tid, $fields_active_filters) {
    $term = taxonomy_get_term($tid);
    $info = array('name' => $term->name, 'description' => $term->description);
    return theme('apachesolr_seo_blocks_term', $info, 'content_taxonomy');
  }

Also you can override the theme function for every type of active filter in
case you are only interested in change the markup but also could work to add
some extra data to the description, for example concatenate the image.

  function cs_apachesolr_seo_blocks_nodereference($info, $field_type) {
    if (empty($info['description'])) {
      return;
    }

    if ($info['node']->type == 'manufacturer' && isset($info['node']->field_manufacturer_logo[0]['filepath'])) {
      $extra = theme('imagecache', 'manufacturer_logos_110x55', $info['node']->field_manufacturer_logo[0]['filepath'], $info['node']->title, $info['node']->title, NULL);
    }

    $info['description'] = $extra . $info['description'];
    
    return apachesolr_seo_blocks_format_description($info);
  }

API
---

function _apachesolr_seo_blocks_active_query_filters
